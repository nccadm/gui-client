# Arquivos #
Nome              | Descrição
----------------- | ----------
**Launcher.py**   | Lançador. Chame o interpretador a partir desse arquivo
**MainWindow.py** | Janela Principal. Carrega o Gtk.Builder a partir do layout.glade
**RowVM.py**      | Classe que herda Gtk.ListBoxRow, contém os dados de cada VM
**Handler.py**    | Classe que manipula os signals do Gtk
**Nomes.py**      | Resolução de algumas ids e caminho -> Pode se tornar um json
**layout.glade**  | Arquivo do Glade contendo o layout da janela principal
**template.json** | Template Json para leitura dos dados das VMs
**icones/**       | Pasta contendo alguns ícones