import gi, os, sys, threading
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class Lancador(threading.Thread):

    def __init__(self, row, campos):
        threading.Thread.__init__(self)
        self.row = row
        self.campos = campos

    def run(self):
        print("Lançando VM "+self.row.dados["id"])
        os.spawnl(os.P_NOWAIT,"/mnt/vms/lanca.sh","lanca.sh", self.row.dados["id"])
        #self.row.switch(True)
        #self.row.defineVM(self.campos)

class Handler():

    def __init__(self, campos):
        self.campos = campos
        self.active = None

    def onRowActivated(self, listBox, rowVM):
        self.active = rowVM
        rowVM.defineVM(self.campos)

    def onCloseWindow(self, *args):
        Gtk.main_quit(*args)
        sys.exit(0)

    def onSwitched(self, switch, gparam):
        on = switch.get_active()
        before = self.active.dados["switch"]
        self.active.switch(on)
        self.onRowActivated(None, self.active)
        if on and not before:
            Lancador(self.active, self.campos).start()
        elif not on and before:
            print("Matando (ou tentando matar) VM "+self.active.dados["id"])

    def onShutDown(self, button):
        print("Iniciando desligamento do sistema...")
        os.system("systemctl poweroff")
