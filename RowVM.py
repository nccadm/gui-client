import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk, GdkPixbuf

class RowVM(Gtk.ListBoxRow):

    def __init__(self, dados):
        Gtk.ListBoxRow.__init__(self)

        # Armazena dados referentes à VM
        self.dados = {
            "nome": dados["nome"],
            "so": dados["so"],
            "mem": dados["memoria"],
            "resp": dados["responsavel"],
            "desc": dados["descricao"],
            "progs": "\n".join(dados["programas"]),
            "ss": dados["ss"],
            "id": dados["id"],
            "distro": dados["distro"],
            "switch": False
        }

        # Cria um grid
        grid = Gtk.Grid()
        self.add(grid)

        # Cria a logo, redimensiona para altura 256 pixeis
        logoSO = Gtk.Image()
        pb = GdkPixbuf.Pixbuf.new_from_file("icones/"+self.dados["distro"]+".png")
        w = (pb.get_width()*100)/pb.get_height()
        spb = pb.scale_simple(w, 100, GdkPixbuf.InterpType.BILINEAR)
        logoSO.set_from_pixbuf(spb)
        logoSO.set_padding(5,5)
        grid.attach(logoSO, 0, 0, 1, 2) # left, top, width, height

        # Cria a label com nome do SO
        nome = Gtk.Label()
        nome.set_text(self.dados["id"])
        nome.set_markup("<span font_desc='Cantarell Bold 16'>%s</span>"%self.dados["id"])
        nome.set_hexpand(True)
        nome.set_vexpand(True)
        nome.set_padding(10,15)
        grid.attach(nome, 1, 0, 2, 1)

        # Cria a label status
        status = Gtk.Label()
        status.set_markup("<span font_desc='Cantarell Bold 14'>Status:</span>")
        status.set_hexpand(False)
        status.set_vexpand(True)
        status.set_padding(5,10)
        status.set_halign(2)
        grid.attach(status, 1, 1, 1, 1)

        # Cria a logo status
        self.icon = {}
        self.statusIcon = Gtk.Image()
        pb = GdkPixbuf.Pixbuf.new_from_file("icones/ic_on.png")
        w = (pb.get_width()*20)/pb.get_height()
        self.icon['on'] = pb.scale_simple(w, 20, GdkPixbuf.InterpType.BILINEAR)
        pb = GdkPixbuf.Pixbuf.new_from_file("icones/ic_off.png")
        self.icon['off'] = pb.scale_simple(w, 20, GdkPixbuf.InterpType.BILINEAR)
        self.statusIcon.set_from_pixbuf(self.icon["off"])
        self.statusIcon.set_halign(1)
        self.statusIcon.set_padding(5,10)
        grid.attach(self.statusIcon, 2, 1, 1, 1)


        # Cria um pixbuf pra screenshot
        pb = GdkPixbuf.Pixbuf.new_from_file(self.dados["ss"])
        w = (pb.get_width()*256)/pb.get_height()
        self.pixbuf = pb.scale_simple(w, 256, GdkPixbuf.InterpType.BILINEAR)

    def defineVM(self, campos):
        # Define os valores da VM para a tela
        for nome in campos.keys():
            if nome == "distro":
                pass
            elif nome == "ss":
                # redimensiona a screenshot
                campos[nome].set_from_pixbuf(self.pixbuf)
            elif nome == "switch":
                # seta o switch
                campos[nome].set_active(self.dados[nome])
                #print("definindo "+nome+" para "+str(self.dados[nome]))
            else:
                # seta a label
                campos[nome].set_text(self.dados[nome])


    def switch(self, status):
        # Troca estado da maquina
        self.dados["switch"] = status
        if status:
            self.statusIcon.set_from_pixbuf(self.icon["on"])
        else:
            self.statusIcon.set_from_pixbuf(self.icon["off"])
