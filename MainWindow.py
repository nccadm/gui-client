# -*- coding: utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from Names import files, names
from Handler import Handler
from RowVM import *
import json
from gi.repository import Gdk

class MainWindow():

    def __init__(self):

        # Carrega o arquivo .glade
        builder = Gtk.Builder()
        builder.add_from_file(files["window"])

        # Carrega os objetos Gtk
        self.window = builder.get_object(names["window"])
        self.lista  = builder.get_object(names["lista"])
        self.campos = {}
        for c in ("nome", "so", "mem", "resp", "desc", "progs", "ss", "switch"):
            self.campos[c] = builder.get_object(names[c])

        # Cria a lista de VMs
        self.rowvms = self.leJSON()
        for row in self.rowvms:
            self.lista.add(row)

        # Define o css
        self.provedor_estilo = Gtk.CssProvider()
        self.provedor_estilo.load_from_data(self.getCss())
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            self.provedor_estilo,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        # Tudo pronto, exibe a janela
        self.window.show_all()

        # Seleciona a primeira linha
        h = Handler(self.campos)
        builder.connect_signals(h)
        h.onRowActivated(self.lista, self.rowvms[0])
        #self.rowvms[0].defineVM(self.campos)
        self.lista.select_row(self.rowvms[0])

        self.window.fullscreen()

    def leJSON(self):
        with open(files["json"]) as jfile:
            dic = json.load(jfile)
        vms = [RowVM(dados) for dados in dic.values()]
        return vms

    def getCss(self):
        with open(files['css'], 'rb') as cssfile:
            css_data = cssfile.read()
        return css_data
